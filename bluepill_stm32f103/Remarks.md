This projects has exposed interesting problems with sharing the same peripheral between multiple threads. I have attempted multiple solutions.
One was based on multiple RegisterBlock variables, which was inherently unsafe.
Now I pass the DMA1 C1 object to the interrupt thread, while RegisterBlock is used in the main thread.
Similar problems are discussed in https://users.rust-lang.org/t/how-can-i-access-my-struct-with-peripherals-in-an-interrupt/94884/8 and in https://users.rust-lang.org/t/how-to-share-peripheral-register-structs-with-interrupt-handlers/29418/11 .

Maybe those solutions should be adpoted here?
