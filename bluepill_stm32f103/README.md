# Simple USB-connected ADC converter for Blackpill written in Rust

This code is published under the dual GPL/BSD License.

The python directory contains the simple libusb-based Python script that configures the device and dumps the acquired data.
You may stop the data acquisition by CTRL+C.


    # ./test3.py
    setting configuration
    config set
    trying to claim device
    claimed device
    device claimed
    OK:CAL
    
    OK:CONFIG
    
    OK:CONFIG
    
    OK:START
    
    D:7e30,b4e0,b4a0,b580,b5d0,6cd0,b530,b7a0,b7d0,b810,7d50,
    D:9ce0,b4b0,b4b0,b590,b5e0,6ce0,b500,b730,b770,b800,8b90,
    D:9700,b360,b480,b590,b5b0,6c90,b540,b760,b7b0,b800,88a0,
    D:9810,b3b0,b4a0,b560,b600,6c70,b500,b710,b770,b7f0,89b0,
    D:9710,b360,b480,b5d0,b5c0,6cc0,b500,b720,b770,b800,88d0,
    D:97e0,b410,b450,b4c0,b5b0,6c90,b500,b710,b760,b820,8990,
    ^CD:96f0,b3a0,b500,b560,b5d0,6cb0,b530,b750,b7b0,b830,88b0,
    OK:STOP

The data acquisition parameters are defined in `period` and `channels` variables in the script.


