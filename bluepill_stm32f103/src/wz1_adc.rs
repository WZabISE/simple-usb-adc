#![allow(missing_docs)]

use panic_semihosting as _;

//use cortex_m_semihosting::hprintln;
use rtic_sync::channel::*;
use usb_device::class_prelude::*;
use usb_device::descriptor::lang_id;
use usb_device::device::{UsbDevice, UsbDeviceBuilder, UsbVidPid};
use usb_device::Result;

use bbqueue::framed::FrameConsumer;

pub mod sizes {
    pub const BBDATA: usize = 8000;
    pub const BBMSGS: usize = 256;
    pub const NOTIFS: usize = 16;
    pub const BUFFER: usize = 256;
    pub const CONTROL_ENDPOINT: u8 = 8;
    pub const BULK_ENDPOINT: u16 = 64;
    pub const MAX_CHANNELS: usize = 16;
    pub const MAX_CHAN_NUM: u8 = 9; // Only 10 channels available in Bluepill
}

/// Test USB class for testing USB driver implementations. Supports various endpoint types and
/// requests for testing USB peripheral drivers on actual hardware.
pub struct WzADC1Class<'a, 'b, B: UsbBus> {
    custom_string: StringIndex,
    interface_string: StringIndex,
    iface: InterfaceNumber,
    ep_bulk_in: EndpointIn<'a, B>,
    ep_bulk_out: EndpointOut<'a, B>,
    bulk_buf_in: [u8; sizes::BUFFER],
    bulk_buf_out: [u8; sizes::BUFFER],
    len: usize,
    i: usize,
    expect_bulk_in_complete: bool,
    expect_bulk_out: bool,
    dta_cons: FrameConsumer<'b, { sizes::BBDATA }>,
    msg_cons: FrameConsumer<'b, { sizes::BBMSGS }>,
    notif_snd: Sender<'static, u8, { sizes::NOTIFS }>,
}

pub const VID: u16 = 0x32ab;
pub const PID: u16 = 0x4a51;
pub const MANUFACTURER: &str = "WZab Software";
pub const PRODUCT: &str = "WZab usb-device WZADC1";
pub const SERIAL_NUMBER: &str = "0001";
pub const CUSTOM_STRING: &str = "WZclass1 Class Custom String";
pub const INTERFACE_STRING: &str = "WZclass1 Interface";

impl<B: UsbBus> WzADC1Class<'_, '_, B> {
    /// Creates a new TestClass.
    pub fn new<'a, 'b>(
        alloc: &'a UsbBusAllocator<B>,
        dta_cons: FrameConsumer<'b, { sizes::BBDATA }>,
        msg_cons: FrameConsumer<'b, { sizes::BBMSGS }>,
        notif_snd: Sender<'static, u8, { sizes::NOTIFS }>,
    ) -> WzADC1Class<'a, 'b, B> {
        WzADC1Class {
            custom_string: alloc.string(),
            interface_string: alloc.string(),
            iface: alloc.interface(),
            ep_bulk_in: alloc.bulk(sizes::BULK_ENDPOINT),
            ep_bulk_out: alloc.bulk(sizes::BULK_ENDPOINT),
            bulk_buf_out: [0; sizes::BUFFER],
            bulk_buf_in: [0; sizes::BUFFER],
            len: 0,
            i: 0,
            expect_bulk_in_complete: false,
            expect_bulk_out: false,
            dta_cons: dta_cons,
            msg_cons: msg_cons,
            notif_snd: notif_snd,
        }
    }

    /// Convenience method to create a UsbDevice that is configured correctly for WzADC1Class.
    pub fn make_device<'a, 'b>(&'a self, usb_bus: &'b UsbBusAllocator<B>) -> UsbDevice<'b, B> {
        self.make_device_builder(usb_bus).build()
    }

    /// Convenience method to create a UsbDeviceBuilder that is configured correctly for TestClass.
    ///
    /// The methods sets
    ///
    /// - manufacturer
    /// - product
    /// - serial number
    /// - max_packet_size_0
    ///
    /// on the returned builder. If you change the manufacturer, product, or serial number fields,
    /// the test host may misbehave.
    pub fn make_device_builder<'a, 'b>(
        &'a self,
        usb_bus: &'b UsbBusAllocator<B>,
    ) -> UsbDeviceBuilder<'b, B> {
        UsbDeviceBuilder::new(&usb_bus, UsbVidPid(VID, PID))
            .manufacturer(MANUFACTURER)
            .product(PRODUCT)
            .device_class(0xff)
            .serial_number(SERIAL_NUMBER)
            .max_packet_size_0(sizes::CONTROL_ENDPOINT)
    }

    /// Must be called after polling the UsbDevice.
    pub fn poll(&mut self) {
        //hprintln!("!");
        if self.expect_bulk_out {
            self.expect_bulk_out = false;
            match self.ep_bulk_out.read(&mut self.bulk_buf_in[0..]) {
                Ok(count) => {
                    //hprintln!(":{}", self.bulk_buf_in[1]);
                    let _ = match self.bulk_buf_in[0] {
                        0 => self.adc_cal(),
                        1 => self.adc_set_chans(count - 1),
                        2 => self.adc_set_period(count - 1),
                        3 => self.adc_start(),
                        4 => self.adc_stop(),
                        _ => self.unknown(), //self.write_str(&mut "I received above 3"), //unknown_command(),
                    };
                }
                Err(UsbError::WouldBlock) => {}
                Err(err) => panic!("bulk read {:?}", err),
            };
        };
    }

    fn unknown(&mut self) -> Result<()> {
        return Ok(());
    }

    fn adc_cal(&mut self) -> Result<()> {
        self.notif_snd.try_send(3).unwrap();
        return Ok(());
    }

    fn adc_start(&mut self) -> Result<()> {
        self.notif_snd.try_send(1).unwrap();
        return Ok(());
    }

    fn adc_stop(&mut self) -> Result<()> {
        self.notif_snd.try_send(2).unwrap();
        return Ok(());
    }

    fn adc_set_chans(&mut self, len: usize) -> Result<()> {
        unsafe {
            crate::ADC_NCHANS = 0;
        };
        //Check if the parameters are OK
        if len >= sizes::MAX_CHANNELS {
            let _ = self.notif_snd.try_send(128); //Too many channels
            return Ok(());
        }
        for i in 0..len {
            if self.bulk_buf_in[i + 1] > sizes::MAX_CHAN_NUM {
                let _ = self.notif_snd.try_send(129); //Invalid channel
                return Ok(());
            }
            unsafe { crate::ADC_CHANS[i] = self.bulk_buf_in[i + 1] };
        }
        unsafe { crate::ADC_NCHANS = (len).try_into().unwrap() };
        self.notif_snd.try_send(0).unwrap();
        return Ok(());
    }

    fn adc_set_period(&mut self, len: usize) -> Result<()> {
        unsafe { crate::ADC_PERIOD = 0 };
        if len != 2 {
            self.notif_snd.try_send(130).unwrap();
            return Ok(());
        }
        let mut tmp: [u8; 2] = [0, 0];
        for i in 0..2 {
            tmp[i] = self.bulk_buf_in[i + 1];
        }
        unsafe { crate::ADC_PERIOD = u16::from_le_bytes(tmp) };
        self.notif_snd.try_send(0).unwrap();
        return Ok(());
    }

    /*
    fn write_msg(&mut self, msg : &[u8], len : usize) -> Result<()> {
      let mut wgrant = self.dta_prod.grant(len).unwrap();
      wgrant.clone_from_slice(msg);
      wgrant.commit(len);
      if !self.expect_bulk_in_complete {
         self.write_bulk_in();
         }
      return Ok(());
    }

    fn write_str(&mut self, msg : &str) -> Result<()> {
      let len = msg.len();
      self.write_msg(msg.as_bytes(),len)
    }
    */

    pub fn write_bulk_in(&mut self) {
        if !self.expect_bulk_in_complete {
            match self.dta_cons.read() {
                Some(rgrant) => {
                    let len = rgrant.len();
                    self.bulk_buf_out[0] = 'D' as u8;
                    self.bulk_buf_out[1] = ':' as u8;
                    for (i, val) in rgrant.iter().enumerate() {
                        self.bulk_buf_out[i + 2] = *val;
                    }
                    rgrant.release();
                    let _ = self.ep_bulk_in.write(&self.bulk_buf_out[0..len + 2]);
                    self.expect_bulk_in_complete = true;
                    return;
                }
                None => {}
            }
            //Sending the messages has lower priority
            match self.msg_cons.read() {
                Some(rgrant) => {
                    let len = rgrant.len();
                    for (i, val) in rgrant.iter().enumerate() {
                        self.bulk_buf_out[i] = *val;
                    }
                    rgrant.release();
                    let _ = self.ep_bulk_in.write(&self.bulk_buf_out[0..len]);
                    self.expect_bulk_in_complete = true;
                    return;
                }
                None => {}
            }
        }
    }
}

impl<B: UsbBus> UsbClass<B> for WzADC1Class<'_, '_, B> {
    fn reset(&mut self) {
        self.len = 0;
        self.i = 0;
        self.expect_bulk_in_complete = false;
        self.expect_bulk_out = false;
    }

    fn get_configuration_descriptors(&self, writer: &mut DescriptorWriter) -> Result<()> {
        writer.interface(self.iface, 0xff, 0x00, 0x00)?;
        writer.endpoint(&self.ep_bulk_in)?;
        writer.endpoint(&self.ep_bulk_out)?;
        writer.interface_alt(self.iface, 1, 0xff, 0x01, 0x00, Some(self.interface_string))?;

        Ok(())
    }

    fn get_string(&self, index: StringIndex, lang_id: u16) -> Option<&str> {
        if lang_id == lang_id::ENGLISH_US {
            if index == self.custom_string {
                return Some(CUSTOM_STRING);
            } else if index == self.interface_string {
                return Some(INTERFACE_STRING);
            }
        }

        None
    }

    fn endpoint_in_complete(&mut self, addr: EndpointAddress) {
        if addr == self.ep_bulk_in.address() {
            if self.expect_bulk_in_complete {
                self.expect_bulk_in_complete = false;
                self.write_bulk_in();
            } else {
                panic!("unexpected endpoint_in_complete");
            }
        }
    }

    fn endpoint_out(&mut self, addr: EndpointAddress) {
        if addr == self.ep_bulk_out.address() {
            self.expect_bulk_out = true;
        }
    }

    fn control_in(&mut self, xfer: ControlIn<B>) {
        let req = *xfer.request();

        if !(req.request_type == control::RequestType::Vendor
            && req.recipient == control::Recipient::Device)
        {
            return;
        }
    }

    fn control_out(&mut self, xfer: ControlOut<B>) {
        let req = *xfer.request();

        if !(req.request_type == control::RequestType::Vendor
            && req.recipient == control::Recipient::Device)
        {
            return;
        }
    }
}
