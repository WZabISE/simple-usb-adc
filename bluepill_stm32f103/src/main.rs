//#![deny(unsafe_code)]
#![no_main]
#![no_std]

use panic_semihosting as _;
mod wz1_adc;
use crate::wz1_adc::WzADC1Class;

use bbqueue::BBBuffer;
static UBUF: BBBuffer<{ wz1_adc::sizes::BBDATA }> = BBBuffer::new();
static MSGBUF: BBBuffer<{ wz1_adc::sizes::BBMSGS }> = BBBuffer::new();
static mut ADC_BUF: [u8; wz1_adc::sizes::MAX_CHANNELS * 2] = [0; wz1_adc::sizes::MAX_CHANNELS * 2];
static mut ADC_CHANS: [u8; wz1_adc::sizes::MAX_CHANNELS] = [0; wz1_adc::sizes::MAX_CHANNELS];
static mut ADC_NCHANS: u8 = 0;
static mut ADC_PERIOD: u16 = 0;

#[rtic::app(device = stm32f1xx_hal::pac, peripherals = true, dispatchers=[SPI2])]
mod app {
    use cortex_m::asm::delay;
    //use cortex_m_semihosting::hprintln;
    use crate::wz1_adc::WzADC1Class;
    use bbqueue::framed::FrameProducer;
    use core::cell::RefCell;
    use cortex_m::interrupt::{self, Mutex};
    use rtic_sync::{channel::*, make_channel};
    use stm32f1xx_hal::adc::{Adc, SampleTime};
    use stm32f1xx_hal::gpio::{Output, Pin};
    use stm32f1xx_hal::pac::adc1::cr2::{DMA_A, EXTSEL_A};
    use stm32f1xx_hal::pac::dma1::ch::cr::{DIR_A, EN_A, MINC_A, MSIZE_A, PINC_A, PSIZE_A, TCIE_A};
    use stm32f1xx_hal::pac::tim2::cr2::MMS_A;
    use stm32f1xx_hal::pac::{ADC1, DMA1, TIM3};
    use stm32f1xx_hal::timer::{Counter, Event};
    use stm32f1xx_hal::usb::{Peripheral, UsbBus, UsbBusType};
    use stm32f1xx_hal::{adc, pac::*, prelude::*};
    use usb_device::prelude::*;

    static DMA1_REGS: Mutex<RefCell<Option<&mut dma1::RegisterBlock>>> =
        Mutex::new(RefCell::new(None));

    #[shared]
    struct Shared {
        usb_dev: UsbDevice<'static, UsbBusType>,
        mydev: WzADC1Class<'static, 'static, UsbBusType>,
    }

    // Local resources go here
    #[local]
    struct Local {
        tim3: Counter<TIM3, 10000>,
        r_tim3: &'static mut tim3::RegisterBlock,
        adc1: Adc<ADC1>,
        dta_prod: FrameProducer<'static, { crate::wz1_adc::sizes::BBDATA }>,
        msg_prod: FrameProducer<'static, { crate::wz1_adc::sizes::BBMSGS }>,
        notif_rcv: Receiver<'static, u8, { crate::wz1_adc::sizes::NOTIFS }>,
        data_snd: Sender<'static, u8, { crate::wz1_adc::sizes::NOTIFS }>,
        msg_snd: Sender<'static, u8, { crate::wz1_adc::sizes::NOTIFS }>,
        bulk_rcv: Receiver<'static, u8, { crate::wz1_adc::sizes::NOTIFS }>,
        led: Pin<'C', 13, Output>,
    }

    #[init]
    fn init(ctx: init::Context) -> (Shared, Local) {
        static mut USB_BUS: Option<usb_device::bus::UsbBusAllocator<UsbBusType>> = None;
        let mut flash = ctx.device.FLASH.constrain();
        let (notif_snd, notif_rcv) = make_channel!(u8, { crate::wz1_adc::sizes::NOTIFS });
        let (bulk_snd, bulk_rcv) = make_channel!(u8, { crate::wz1_adc::sizes::NOTIFS });
        let data_snd = bulk_snd.clone();
        let msg_snd = bulk_snd.clone();
        let rcc = ctx.device.RCC.constrain();
        let (dta_prod, dta_cons) = crate::UBUF.try_split_framed().unwrap();
        let (msg_prod, msg_cons) = crate::MSGBUF.try_split_framed().unwrap();
        let clocks = rcc
            .cfgr
            .use_hse(8.MHz())
            .sysclk(48.MHz())
            .adcclk(12.MHz())
            .pclk1(24.MHz())
            .freeze(&mut flash.acr);

        assert!(clocks.usbclk_valid());
        //hprintln!("Starting USB");

        let mut gpioa = ctx.device.GPIOA.split();
        let mut gpiob = ctx.device.GPIOB.split();
        let mut gpioc = ctx.device.GPIOC.split();
        let led = gpioc.pc13.into_push_pull_output(&mut gpioc.crh);
        // Prepare the list of analog pins available in bluepill (10 first channels!)
        gpioa.pa0.into_analog(&mut gpioa.crl);
        gpioa.pa1.into_analog(&mut gpioa.crl);
        gpioa.pa2.into_analog(&mut gpioa.crl);
        gpioa.pa3.into_analog(&mut gpioa.crl);
        gpioa.pa4.into_analog(&mut gpioa.crl);
        gpioa.pa5.into_analog(&mut gpioa.crl);
        gpioa.pa6.into_analog(&mut gpioa.crl);
        gpioa.pa7.into_analog(&mut gpioa.crl);
        gpiob.pb0.into_analog(&mut gpiob.crl);
        gpiob.pb1.into_analog(&mut gpiob.crl);

        // We need initialize ADC and DMA with HAL, before giving
        // access to its registers.
        let adc1 = adc::Adc::adc1(ctx.device.ADC1, clocks);
        // We enable DMA1 channel 1
        let _dma_ch1 = ctx.device.DMA1.split().1;
        interrupt::free(|cs| {
            DMA1_REGS
                .borrow(cs)
                .replace(Some(unsafe { &mut *(DMA1::ptr() as *mut _) }))
        });
        // We prepare timer 3 for triggering ADC
        let tim3 = ctx.device.TIM3.counter(&clocks);
        let r_tim3 = unsafe { &mut *(TIM3::ptr() as *mut _) };
        // BluePill board has a pull-up resistor on the D+ line.
        // Pull the D+ pin down to send a RESET condition to the USB bus.
        // This forced reset is needed only for development, without it host
        // will not reset your device when you upload new firmware.
        let mut usb_dp = gpioa.pa12.into_push_pull_output(&mut gpioa.crh);
        usb_dp.set_low();
        delay(clocks.sysclk().raw() / 100);

        let usb_dm = gpioa.pa11;
        let usb_dp = usb_dp.into_floating_input(&mut gpioa.crh);

        let usb = Peripheral {
            usb: ctx.device.USB,
            pin_dm: usb_dm,
            pin_dp: usb_dp,
        };

        unsafe {
            USB_BUS.replace(UsbBus::new(usb));
        };

        let mydev = WzADC1Class::new(
            unsafe { USB_BUS.as_ref().unwrap() },
            dta_cons,
            msg_cons,
            notif_snd,
        );

        let usb_dev = mydev.make_device(unsafe { USB_BUS.as_ref().unwrap() });
        acquire_data::spawn().unwrap();
        data_transfer::spawn().unwrap();

        (
            Shared {
                usb_dev,
                mydev,
                // Initialization of shared resources go here
            },
            Local {
                // Initialization of local resources go here
                tim3,
                r_tim3,
                adc1,
                dta_prod,
                msg_prod,
                //notif_snd,
                notif_rcv,
                data_snd,
                msg_snd,
                bulk_rcv,
                led,
            },
            //init::Monotonics()
        )
    }

    #[task(binds = USB_HP_CAN_TX, shared = [usb_dev, mydev])]
    fn usb_tx(cx: usb_tx::Context) {
        let mut usb_dev = cx.shared.usb_dev;
        let mut mydev = cx.shared.mydev;

        (&mut usb_dev, &mut mydev).lock(|usb_dev, mydev| {
            super::usb_poll(usb_dev, mydev);
        });
    }

    #[task(binds = USB_LP_CAN_RX0, shared = [usb_dev, mydev])]
    fn usb_rx0(cx: usb_rx0::Context) {
        let mut usb_dev = cx.shared.usb_dev;
        let mut mydev = cx.shared.mydev;

        (&mut usb_dev, &mut mydev).lock(|usb_dev, mydev| {
            super::usb_poll(usb_dev, mydev);
        });
    }

    #[task(binds = TIM3, local = [led,r_tim3])]
    fn tim_upd(cx: tim_upd::Context) {
        let tim_upd::LocalResources { led, r_tim3, .. } = cx.local;
        //Toggle LED
        led.toggle();
        r_tim3.sr.write(|w| w.uif().clear_bit());
    }

    // The task below gets activated when DMA ends transfer of the data.
    // It should copy the received data to the bbbuffer.
    // However it must also know how many data are transferred...
    // So I'll need to add an additional shared variable for that...
    #[task(binds = DMA1_CHANNEL1, local = [dta_prod,data_snd])]
    fn dma_eot(cx: dma_eot::Context) {
        let dma_eot::LocalResources {
            dta_prod,
            //led,
            data_snd,
            ..
        } = cx.local;
        //Clear the interrupt flag
        interrupt::free(|cs| {
            let rd1 = DMA1_REGS.borrow(cs).borrow();
            let r_dma1 = rd1.as_ref().unwrap();
            r_dma1.ifcr.write(|w| w.ctcif1().clear());
        });
        //The code below requires correction!
        unsafe {
            let mut wgrant = dta_prod.grant(usize::from(crate::ADC_NCHANS * 2)).unwrap();
            wgrant.clone_from_slice(&crate::ADC_BUF[0..usize::from(crate::ADC_NCHANS * 2)]);
            wgrant.commit(usize::from(crate::ADC_NCHANS * 2));
        };
        //And here we should trigger sending the data from the buffer...
        data_snd.try_send(1).unwrap();
    }

    //The task below just performs the USB data transfer outside the interrupt context
    #[task(priority = 1, local = [bulk_rcv], shared = [mydev] )]
    async fn data_transfer(cx: data_transfer::Context) {
        let data_transfer::LocalResources { bulk_rcv, .. } = cx.local;
        let data_transfer::SharedResources { mut mydev, .. } = cx.shared;
        loop {
            let _req = bulk_rcv.recv().await;
            (&mut mydev).lock(|mydev| {
                mydev.write_bulk_in();
            });
        }
    }

    //The task below is started when the system gets initialized,
    #[task(priority = 1, local = [tim3, adc1, msg_prod, msg_snd, notif_rcv])]
    async fn acquire_data(cx: acquire_data::Context) {
        let acquire_data::LocalResources {
            tim3,
            adc1,
            notif_rcv,
            msg_prod,
            msg_snd,
            ..
        } = cx.local;

        // The internal function for writing the message to the USB endpoint
        fn write_msg(
            prod: &mut FrameProducer<'static, { crate::wz1_adc::sizes::BBMSGS }>,
            snd: &mut Sender<'static, u8, { crate::wz1_adc::sizes::NOTIFS }>,
            msg: &str,
        ) {
            let len = msg.len();
            let bmsg = msg.as_bytes();
            let mut wgrant = prod.grant(len).unwrap();
            wgrant.clone_from_slice(bmsg);
            wgrant.commit(len);
            let _ = snd.try_send(1);
        }

        // Perform initial configuration
        // Configure DMA for working with our circular buffer
        // Configure ADC for working with DMA
        // Here is a nice guide: https://controllerstech.com/dma-with-adc-using-registers-in-stm32/
        // Names of registers and values found in
        // https://docs.rs/stm32f1/latest/stm32f1/stm32f103/dma1/ch/index.html
        // and its subpages
        // and in
        // https://docs.rs/stm32f1/latest/stm32f1/stm32f103/adc1/index.html
        // and its subpages
        let r_adc1: &adc1::RegisterBlock = unsafe { &mut *(ADC1::ptr() as *mut _) };
        //let mut chans: Vec<u8, { crate::wz1_adc::sizes::MAX_CHANNELS }> = Vec::new();
        //chans.extend_from_slice(&[0, 1, 3, 3, 2]).unwrap();
        loop {
            // Wait for command from notifications channel
            let req = notif_rcv.recv().await;
            match req {
                Ok(0) => {
                    write_msg(msg_prod, msg_snd, "OK:CONFIG\n");
                }
                Ok(1) => {
                    //hprintln!("starting!");
                    // Just start the acquisition
                    // Disable ADC
                    // Check if channels and period are configured
                    if unsafe { crate::ADC_NCHANS == 0 } {
                        write_msg(msg_prod, msg_snd, "ERR:CHANNELS NOT SET\n");
                        continue;
                    }
                    if unsafe { crate::ADC_PERIOD == 0 } {
                        write_msg(msg_prod, msg_snd, "ERR:PERIOD NOT SET\n");
                        continue;
                    }
                    interrupt::free(|cs| {
                        let rd1 = DMA1_REGS.borrow(cs).borrow();
                        let r_dma1 = rd1.as_ref().unwrap();
                        r_dma1.ch1.cr.modify(|_, w| w.en().variant(EN_A::Disabled));
                        // We program the sequence of sampled channels
                        r_adc1.cr2.modify(|_, w| {
                            w.adon()
                                .set_bit()
                                .dma()
                                .clear_bit()
                                .cont()
                                .clear_bit()
                                .align()
                                .bit(true)
                        });
                        unsafe {
                            for i in 0..crate::ADC_NCHANS {
                                adc1.set_channel_sample_time(
                                    crate::ADC_CHANS[usize::from(i)],
                                    SampleTime::T_28,
                                );
                            }
                            adc1.set_regular_sequence(
                                &crate::ADC_CHANS[0..usize::from(crate::ADC_NCHANS)],
                            );
                        };
                        r_adc1
                            .cr1
                            .modify(|_, w| w.scan().set_bit().discen().clear_bit());
                        r_adc1.cr2.modify(|_, w| {
                            w.dma()
                                .set_bit()
                                .extsel()
                                .variant(EXTSEL_A::Tim3trgo)
                                .exttrig()
                                .set_bit()
                        });
                        // Now program the DMA
                        r_dma1.ch1.cr.modify(|_, w| {
                            w.circ()
                                .set_bit()
                                .pinc()
                                .variant(PINC_A::Disabled)
                                .minc()
                                .variant(MINC_A::Enabled)
                                .psize()
                                .variant(PSIZE_A::Bits16)
                                .msize()
                                .variant(MSIZE_A::Bits16)
                                .dir()
                                .variant(DIR_A::FromPeripheral)
                        });
                        // set the source address: buffer address
                        r_dma1
                            .ch1
                            .mar
                            .modify(|_, w| unsafe { w.ma().bits(crate::ADC_BUF.as_ptr() as u32) });
                        // set the destintaion address: ADC1 12-bit right-aligned data holding register (ADCx_DR)
                        r_dma1
                            .ch1
                            .par
                            .modify(|_, w| unsafe { w.pa().bits(&r_adc1.dr as *const _ as u32) });
                        // set the number of data words
                        r_dma1
                            .ch1
                            .ndtr
                            .modify(|_, w| w.ndt().bits(unsafe { crate::ADC_NCHANS as u16 }));
                        // enable transfer complete interrupt
                        r_dma1
                            .ch1
                            .cr
                            .modify(|_, w| w.tcie().variant(TCIE_A::Enabled));
                        // enable DMA for ADC
                        r_adc1.cr2.modify(|_, w| w.dma().variant(DMA_A::Enabled));
                        r_dma1.ch1.cr.modify(|_, w| w.en().variant(EN_A::Enabled));
                    });
                    // Prepare trigger 3 for operation
                    let _ = tim3.start(unsafe { ((crate::ADC_PERIOD as u32) * 100).micros() });
                    tim3.set_master_mode(MMS_A::Update);
                    tim3.listen(Event::Update);
                    write_msg(msg_prod, msg_snd, "OK:START\n");
                    //hprintln!("starting");
                }
                Ok(2) => {
                    // Just stop the acquisition
                    let _ = tim3.cancel();
                    interrupt::free(|cs| {
                        let rd1 = DMA1_REGS.borrow(cs).borrow();
                        let r_dma1 = rd1.as_ref().unwrap();
                        r_dma1.ch1.cr.modify(|_, w| w.en().variant(EN_A::Disabled));
                        r_adc1.cr2.modify(|_, w| w.adon().clear_bit());
                    });
                    //hprintln!("stopping");
                    write_msg(msg_prod, msg_snd, "OK:STOP\n");
                }
                Ok(3) => {
                    //hprintln!("calibration start");
                    r_adc1.cr2.modify(|_, w| w.adon().set_bit());
                    delay(100);
                    r_adc1.cr2.modify(|_, w| w.cal().set_bit());
                    loop {
                        if r_adc1.cr2.read().cal().is_complete() {
                            break;
                        }
                    }
                    //hprintln!("calibration finished");
                    write_msg(msg_prod, msg_snd, "OK:CAL\n");
                }
                Ok(128) => {
                    write_msg(msg_prod, msg_snd, "ERR:TOO MANY CHANNELS\n");
                }
                Ok(129) => {
                    write_msg(msg_prod, msg_snd, "ERR:INVALID CHANNEL\n");
                }

                _ => {
                    write_msg(msg_prod, msg_snd, "ERR:UNKNOWN\n");
                    //hprintln!("Something strange received: {:?}", req);
                }
            }
        }
    }

    // Optional idle, can be removed if not needed.
    #[idle]
    fn idle(_: idle::Context) -> ! {
        loop {
            continue;
        }
    }
}

fn usb_poll<B: usb_device::bus::UsbBus>(
    usb_dev: &mut usb_device::prelude::UsbDevice<'static, B>,
    mydev: &mut WzADC1Class<'static, 'static, B>,
) {
    //hprintln!("p");
    if usb_dev.poll(&mut [mydev]) {
        mydev.poll(); // It is not called by the usb_dev.poll ?!
    }
    //mydev.write_bulk_in();
}
