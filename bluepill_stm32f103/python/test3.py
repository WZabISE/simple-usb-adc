#!/usr/bin/env python
# -*- encoding=iso-8859-2 -*-
# Written by Wojciech M. Zabołotny <wzab01@gmail.com>
# Copyleft 2022 W.M. Zabołotny
# This is a PUBLIC DOMAIN code
#
# The code is somehow based on:
# https://stackoverflow.com/questions/44290837/how-to-interact-with-usb-device-using-pyusb

period = 1.0
channels = [0,1,2,3,4,5,6,7,8,9,0]

import usb.core
import usb.util
import threading
import struct
import signal
import time

# Globals are kept in a single variable 
# That trick enables accessing them from 
# various routines...

class glbs:
  pass
glb = glbs()

glb.runplot = True
glb.runrcv = True

def on_sig_int(sig,frame):
   glb.runplot = False

signal.signal(signal.SIGINT, on_sig_int)

# Command for printting the status or the data
def resp(msg):
   # Is this data?
   if bytes(msg[0:2])==b"D:":
      res="D:"
      for i in range(2,len(msg)-1,2):
        res += format(struct.unpack("<H",msg[i:(i+2)])[0],"04x")+","
      print(res)
   else:
      print(bytes(msg).decode())

# find our device
dev = usb.core.find(idVendor=0x32ab, idProduct=0x4a51)

# was it found?
if dev is None:
    raise ValueError('Device not found')

# free up the device from the kernel
for cfg in dev:
    for intf in cfg:
        if dev.is_kernel_driver_active(intf.bInterfaceNumber):
            try:
                dev.detach_kernel_driver(intf.bInterfaceNumber)
            except usb.core.USBError as e:
                sys.exit("Could not detach kernel driver from interface({0}): {1}".format(intf.bInterfaceNumber, str(e)))

# try default conf
print("setting configuration")
dev.set_configuration()
print("config set")

print("trying to claim device")
try:
    usb.util.claim_interface(dev, 0)
    print("claimed device")
except usb.core.USBError as e:
    print("Error occurred claiming " + str(e))
    sys.exit("Error occurred on claiming")
print("device claimed")
cfg=dev.get_active_configuration()
intf=cfg.interfaces()
glb.eps=intf[0].endpoints()
eps=intf[0].endpoints()

# Stop the conversion
eps[1].write(b"\x04") 
# Print this response and possible old responses
try:
    while True:
      resp(eps[0].read(300,timeout=500))
except Exception as e:
      pass
# Do calibration
eps[1].write(b"\x00")
resp(eps[0].read(300,timeout=10000))

# Set period (in seconds)
if period > 6.5535:
   rise(Exception("Too long period. Must be up to 6.5535 [s]"))
period = int(period/1e-4)
cmd = struct.pack("<BH",0x02,period)       
eps[1].write(cmd)
resp(eps[0].read(300,timeout=10000))

# Set channels 
cmd = bytes([1,]+channels) 
eps[1].write(cmd)
resp(eps[0].read(300,timeout=10000))

# Start conversion
eps[1].write(b"\x03")
resp(eps[0].read(300,timeout=10000))

# Print results
while glb.runplot:
  resp(eps[0].read(300,timeout=10000))

#Stop acquisition
eps[1].write(b"\x04")
resp(eps[0].read(300,timeout=10000))

